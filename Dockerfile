FROM python:3.9-slim-buster

LABEL Name="Test-Repo" Version=1.0.0
LABEL org.opencontainers.image.source = "https://gitlab.rrze.fau.de/ci46mare/test-repo"

ARG srcDir=src
WORKDIR /src
COPY $srcDir/requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY $srcDir/ ./src

EXPOSE 5000

# The server can be started from here with a flask compatible server
# CMD ["gunicorn", "-b", "0.0.0.0:5000", "run:src"]