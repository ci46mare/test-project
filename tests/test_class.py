# From [tutorial](https://docs.pytest.org/en/7.1.x/getting-started.html)
# pytest tests all files containing test_*.py or *_test.py
# mind that pytest needs __init__.py files in the directories to work (they are used to be identified as python directories)

import pytest
from src.helloworld import hello_world

class TestClass:
    # Test if file ../src/helloworld.py returns "Hello World!"
    def test_hello_world(self):
        assert hello_world() == "Hello World!"
    